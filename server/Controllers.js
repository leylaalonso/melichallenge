import MeliServices from './MeliServices';

const author = {
  name: 'Leyla',
  lastname: 'Alonso',
};

const formatCategories = (filters) => {
  const category = filters.find((f) => f.id === 'category');
  if (!category) {
    return [];
  }
  const categoryValue = category.values[0];
  if (!categoryValue) {
    return [];
  }
  return categoryValue.path_from_root.map((p) => p.name);
};

const formatItems = (results) => results.map((r) => ({
  id: r.id,
  title: r.title,
  price: {
    currency: r.currency_id,
    amount: r.price,
    decimals: Math.floor((r.price - Math.floor(r.price)) * 100),
  },
  picture: r.thumbnail,
  condition: r.condition,
  free_shipping: r.free_shipping,
}));

const formatItem = async (item) => {
  const description = await MeliServices.getItemDescription(item.id);
  return {
    id: item.id,
    title: item.title,
    price: {
      currency: item.currency_id,
      amount: item.price,
      decimals: Math.floor((item.price - Math.floor(item.price)) * 100),
    },
    picture: item.pictures[0].url,
    condition: item.condition,
    free_shipping: item.shipping.free_shipping,
    sold_quantity: item.sold_quantity,
    description: description.plain_text,
  };
};

const findItems = async (search) => {
  const resp = await MeliServices.getItems(search, 4, 0);
  return {
    author,
    categories: formatCategories(resp.filters),
    items: formatItems(resp.results),
  };
};

const getItem = async (itemId) => {
  const resp = await MeliServices.getItem(itemId);
  const item = await formatItem(resp);
  return {
    author,
    item,
  };
};

const Controllers = {
  findItems,
  getItem,
};

export default Controllers;
