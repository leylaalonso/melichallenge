/* eslint-disable no-console */
import express from 'express';
import dotenv from 'dotenv';
import cors from 'cors';
import Controllers from './Controllers';

dotenv.config();

const server = express();

server.use(express.json());
server.use(cors());

server.get('/api/items', async (req, res) => {
  try {
    const response = await Controllers.findItems(req.query.search);
    res.json(response);
  } catch (e) {
    console.error(e);
    res.status(500).send('ERROR');
  }
});

server.get('/api/items/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const response = await Controllers.getItem(id);
    res.json(response);
  } catch (e) {
    console.error(e);
    res.status(500).send('ERROR');
  }
});

const port = process.env.SERVER_PORT || 8080;

server.listen(port, () => {
  console.log(`server UP at ${port}`);
});
