import Axios from 'axios';

const getItems = async (q, limit, offset) => {
  const params = {
    q,
    limit,
    offset,
  };
  const baseURL = 'https://api.mercadolibre.com';
  const url = '/sites/MLA/search';
  const response = await Axios({
    baseURL,
    params,
    url,
  });
  return response.data;
};

const getItem = async (id) => {
  const baseURL = 'https://api.mercadolibre.com';
  const url = `/items/${id}`;
  const response = await Axios({
    baseURL,
    url,
  });
  return response.data;
};

const getItemDescription = async (id) => {
  const baseURL = 'https://api.mercadolibre.com';
  const url = `/items/${id}/description`;
  const response = await Axios({
    baseURL,
    url,
  });
  return response.data;
};

const MeliServices = {
  getItem,
  getItems,
  getItemDescription,
};

export default MeliServices;
