# Meli Challenge

Author: Leyla Alonso

## Requirements

* Node ^12

## Installation

`npm i` installs all the dependencies needed

## Available Scripts

In the project directory, you need to run:

### `npm run start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm run server`

Launches the back-end server.<br />

## Enviroment

you can change the enviroment variables from the `.env` file

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
