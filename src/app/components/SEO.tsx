import React from 'react';
import { Helmet } from 'react-helmet';
import { ItemDetailResult } from '../model/SearchResult';

interface Props {
  itemDetail: ItemDetailResult;
}

const SEO = ({ itemDetail }: Props) => (
  <Helmet
    htmlAttributes={{
      lang: 'ES',
    }}
    title="MercadoLibre"
    titleTemplate={`%s | ${itemDetail.item.title}`}
    meta={[
      {
        name: 'description',
        content: itemDetail.item.description,
      },
      {
        property: 'og:title',
        content: itemDetail.item.title,
      },
      {
        property: 'og:description',
        content: itemDetail.item.description,
      },
      {
        property: 'og:type',
        content: 'website',
      },
      {
        name: 'twitter:card',
        content: 'summary',
      },
      {
        name: 'twitter:creator',
        content: `${itemDetail.author.lasname}, ${itemDetail.author.name}`,
      },
      {
        name: 'twitter:title',
        content: itemDetail.item.title,
      },
      {
        name: 'twitter:description',
        content: itemDetail.item.description,
      },
    ]}
  />
);
export default SEO;
