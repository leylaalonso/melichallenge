import React from 'react';

interface Props {
  value: {
    amount: number,
    decimals: number,
    currency: string,
  }
}

// Robar es malo
const getFormattedIntFraction = (price : number, separator = '.') => {
  const rawFraction = Math.floor(price).toString();
  return rawFraction.replace(
    /(\d)(?=(\d\d\d)+(?!\d))/g,
    (n) => `${n}${separator}`,
  );
};

const getFormattedDecimal = (decimal : number) => decimal.toString().padStart(2, '0');

const getFormattedCurrencie = (currency: string) => {
  switch (currency) {
    case 'ARS': return '$';
    case 'USD': return 'u$s';
    case 'BRL': return 'R$';
    default: return '?$';
  }
};

const Price = ({ value }: Props) => (
  <div className="price">
    {getFormattedCurrencie(value.currency)}
    {getFormattedIntFraction(value.amount)}
    {
      value.decimals > 0 && (
        <sup>{getFormattedDecimal(value.decimals)}</sup>
      )
    }
  </div>
);

export default Price;
