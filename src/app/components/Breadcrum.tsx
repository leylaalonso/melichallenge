import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleRight } from '@fortawesome/free-solid-svg-icons';

interface Props {
  categories:string[]
}

const Breadcrum = ({ categories }: Props) => (
  <ul className="breadcrums">
    {
    categories.map((c, i) => (
      <React.Fragment key={c}>
        { i > 0 && <li><FontAwesomeIcon icon={faAngleRight} /></li> }
        <li>{c}</li>
      </React.Fragment>
    ))
    }
  </ul>
);

export default Breadcrum;
