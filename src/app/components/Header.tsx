import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useRef } from 'react';
import { Link, useHistory } from 'react-router-dom';

interface Props {
}

const Header = () => {
  const searchInput = useRef<HTMLInputElement>(null);
  const history = useHistory();

  const onSubmit = (e : React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const queryParam = new URLSearchParams({ search: searchInput.current?.value ?? '' }).toString();
    history.push({ pathname: '/items', search: `?${queryParam}` });
  };

  return (
    <header>
      <nav className="nav">
        <Link to="/" className="nav-logo" />
        <form className="search-bar" onSubmit={onSubmit}>
          <input type="text" className="nav-search-input" ref={searchInput} />
          <button type="submit">
            <FontAwesomeIcon icon={faSearch} />
          </button>
        </form>
      </nav>
    </header>
  );
};

export default Header;
