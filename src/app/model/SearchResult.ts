/* eslint-disable camelcase */
export interface SearchResult{
  author:Credentials,
  categories: string[],
  items: ItemSearch[]
}

export interface ItemDetailResult{
  author:Credentials,
  item: ItemDetail,
}

export interface Credentials{
  name: string,
  lasname:string
}

interface ItemSearch{
  id: string,
  title: string,
  price: Price,
  picture: string,
  condition: string,
  free_shipping: boolean
}

interface ItemDetail{
  id: string,
  title: string,
  price: Price,
  picture: string,
  condition: string,
  free_shipping: boolean,
  sold_quantity: number,
  description: string
}

interface Price {
  currency: string,
  amount: number,
  decimals: number
}
