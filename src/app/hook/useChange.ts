import { useEffect } from 'react';

const useChange = (onChange: ()=>void, deps : React.DependencyList) => {
  useEffect(() => {
    onChange();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, deps);
};

export default useChange;
