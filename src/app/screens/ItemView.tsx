import React, { useState } from 'react';
import { useRouteMatch } from 'react-router-dom';
import Price from '../components/Price';
import Template from '../components/Template';
import useChange from '../hook/useChange';
import { ItemDetailResult } from '../model/SearchResult';
import MeliChallengeApi from '../services/MeliChallengeApi';
import SEO from '../components/SEO';

const getFormatedCondition = (condition: string) => {
  switch (condition) {
    case 'new': return 'Nuevo';
    case 'used': return 'Usado';
    default: return condition;
  }
};

const ItemView = () => {
  const [itemDetail, setItemDetail] = useState<ItemDetailResult|undefined>(undefined);
  const match = useRouteMatch<{id:string}>();

  useChange(async () => {
    const itemId = match.params.id;
    const result = await MeliChallengeApi.getItemDetail(itemId);
    setItemDetail(result);
  }, [match.params]);

  if (!itemDetail) {
    return null;
  }

  return (
    <Template>
      <SEO itemDetail={itemDetail} />
      <div className="container">
        <section className="item-result">
          <div className="top">
            <img src={itemDetail.item.picture} alt={itemDetail.item.title} />
            <div className="info">
              <div>
                {getFormatedCondition(itemDetail.item.condition)}
                {' '}
                -
                {' '}
                {itemDetail.item.sold_quantity}
                {' '}
                vendidos
              </div>
              <div className="title">{itemDetail.item.title}</div>
              <Price value={itemDetail.item.price} />
              <button className="primary" type="button">Comprar</button>
            </div>
          </div>
          <div className="description">
            <h2>Descripción del producto</h2>
            <p>
              {itemDetail.item.description.split('\n').map((p, i) => (
                // eslint-disable-next-line react/no-array-index-key
                <React.Fragment key={i}>
                  {p}
                  <br />
                </React.Fragment>
              ))}
            </p>
          </div>
        </section>
      </div>
    </Template>
  );
};

export default ItemView;
