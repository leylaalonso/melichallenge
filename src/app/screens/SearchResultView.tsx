import React, { useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import useChange from '../hook/useChange';
import { SearchResult } from '../model/SearchResult';
import MeliChallengeApi from '../services/MeliChallengeApi';
import Breadcrum from '../components/Breadcrum';
import Template from '../components/Template';
import Price from '../components/Price';

const SearchResultView = () => {
  const [searchResult, setSearchResult] = useState<SearchResult|undefined>(undefined);
  const location = useLocation();

  useChange(async () => {
    const params = new URLSearchParams(location.search);
    const searchParam = params.get('search');
    if (searchParam !== null) {
      const results = await MeliChallengeApi.getItems(searchParam);
      setSearchResult(results);
    }
  }, [location.search]);

  if (!searchResult) {
    return null;
  }
  return (
    <Template>
      <div className="container">
        <Breadcrum categories={searchResult.categories} />
        <ul className="search-result">
          {
          searchResult.items.map((i) => (
            <li key={i.id}>
              <Link to={`/items/${i.id}`}>
                <img src={i.picture} alt="" />
                <div className="info">
                  <Price value={i.price} />
                  <div className="title">{i.title}</div>
                </div>
              </Link>
            </li>
          ))
        }
        </ul>
      </div>
    </Template>
  );
};

export default SearchResultView;
