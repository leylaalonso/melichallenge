import { SearchResult, ItemDetailResult } from '../model/SearchResult';
import api from '../config/api';

export default class MeliChallengeApi {
  static async getItems(search: string) : Promise<SearchResult> {
    const response = await api.get<SearchResult>('/items', { params: { search } });
    return response.data;
  }

  static async getItemDetail(id: string) : Promise<ItemDetailResult> {
    const response = await api.get<ItemDetailResult>(`/items/${id}`);
    return response.data;
  }
}
