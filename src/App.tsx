import React from 'react';
import './App.scss';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './app/screens/Home';
import SearchResultView from './app/screens/SearchResultView';
import ItemView from './app/screens/ItemView';

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/items" component={SearchResultView} />
        <Route exact path="/items/:id" component={ItemView} />
        <Route>
          <h1>404</h1>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
